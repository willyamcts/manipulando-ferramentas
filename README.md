## Atividade
Atividade de Gerenciamento e Configura��o de Software - Engenharia de Software - Fase 4 - Cat�lica SC 2018/02


```
#!java

import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;

public class TelaTriangulo {
	public static void main(String args[]) {
	
		Scanner sc = new Scanner(System.in);
		System.out.println("Informa o tipo do triangulo");
		System.out.println("Informa o valor do lado a");
		double a = sc.nextDouble();
		System.out.println("Informa o valor do lado b");
		double b = sc.nextDouble();
		System.out.println("Informa o valor do lado c");
		double c = sc.nextDouble();

		Set<String> propriedades = new HashSet<String>();
		double hipotenusa = a;

		if (hipotenusa < b) {
		hipotenusa = b;
		}

		if (hipotenusa < c) {
		hipotenusa = c;
		}

		if ((a + b + c - hipotenusa) > hipotenusa) {
			propriedades.add("V�lido");

				if (a == b || b == c || c == a) {
					if (a == b && b == c) {
						propriedades.add("Equil�tero");
					} else {
						propriedades.add("Is�sceles");
					}
				}

			if (a != b && b != c && c != a) {
				propriedades.add("Escaleno");
			}

			if (((a * a + b * b + c * c) - hipotenusa * hipotenusa) ==
				hipotenusa
					* hipotenusa) {
				propriedades.add("Ret�ngulo");
			}
		} else {
		propriedades.add("Inv�lido");
		}

		System.out.println("Propriedades do triangulo ->");
		for (String valor : propriedades) {
		System.out.println(valor);
		}
	}
}

```


1- Use o Eclipse Java Oxygen/Netbeans ou a ferramenta que desejar


2- Crie uma conta no Bitbucket/GitHub ou ... O c�digo deve estar em um Git na Nuvem do reposit�rio escolhido 
	- [Versionando com Git - mateuspaduaweb](http://www.mateuspaduaweb.com.br/versionando-com-o-git-utilizando-o-bitbucket-como-repositorio-remoto/)
	- [Aprenda utilizar o bitbucket](https://www.upinside.com.br/blog/aprenda-a-utilizar-o-bitbucket-como-repositorio-remoto-parte-1)


3- Crie uma branch para fazer melhorias no c�digo (siga as pr�ticas do GitFlow)


4- Crie uma classe para o Triangulo e tire o c�digo de neg�cio que esta no m�todo main e leve para nova classe


5- Criar testes unit�rios para o c�digo desenvolvido


6- Estudar Complexidade Ciclom�tica
	- https://pt.wikipedia.org/wiki/Complexidade_ciclom%C3%A1tica
	- http://blog.caelum.com.br/medindo-a-complexidade-do-seu-codigo/
	- https://www.treinaweb.com.br/blog/complexidade-ciclomatica-analise-estatica-e-refatoracao/


7- Criar em um Wiki (por exemplo do Bitbucket) uma p�gina resumindo o que voc� entendeu sobre Complexidade ciclom�tica


8- Analisar a complexidade do seu c�digo usando o site http://www.lizard.ws/ ou outra ferramenta que preferir (ex. SonarCloud - https://sonarcloud.io/about ) e refatore o c�digo desenvolvido de forma a diminuir a complexidade ciclom�tica da sua solu��o.


9- Adicionar o seu c�digo a integra��o cont�nua Pode usar Bitbucket (Deployments), Circle CI, Travis CI, Jenkins ou a ferramenta que desejar�


10- Entregar evidencias de que a tarefa foi feita (ex. link de reposit�rio p�blico ou documento word descrevendo o que fez)